import 'package:flutter/material.dart';

extension ContextExtensions on BuildContext {
  /// Sizes
  MediaQueryData get mq => MediaQuery.of(this);

  double get screenWidth => mq.size.width;

  double get screenHeight => mq.size.height;

  double get topPadding => mq.padding.top;
}
