import 'package:get_it/get_it.dart';
import 'package:video_list_app/features/video/data/data_sources/mock/data_providers.dart';
import 'package:video_list_app/features/video/data/repositories/video_repository_impl.dart';
import 'package:video_list_app/features/video/domain/repositories/video_base_repository.dart';
import 'package:video_list_app/features/video/domain/use_cases/get_video_use_case.dart';
import 'package:video_list_app/features/video/domain/use_cases/set_like_video_use_case.dart';
import 'package:video_list_app/features/video/domain/use_cases/unset_like_video_use_case.dart';

final sl = GetIt.instance;

setupServiceLocator() async {
  // Videos
  sl
    ..registerFactory<LikesDataProvider>(() => const LikesDataProviderMock())
    ..registerFactory<VideosDataProvider>(() => const VideosDataProviderMock())
    ..registerFactory<VideoBaseRepository>(() => VideoRepositoryImpl(videosDataProvider: sl(), likesDataProvider: sl()))

    // use cases
    ..registerLazySingleton<FetchVideoUseCase>(() => FetchVideoUseCase(sl()))
    ..registerLazySingleton<SetLikeVideoUseCase>(
        () => SetLikeVideoUseCase(sl()))
    ..registerLazySingleton<UnSetLikeVideoUseCase>(
        () => UnSetLikeVideoUseCase(sl()));
}
