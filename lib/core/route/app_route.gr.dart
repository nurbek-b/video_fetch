// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'app_route.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    VideoDetailRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const VideoDetailScreen(),
      );
    },
    VideoListRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const VideoListScreen(),
      );
    },
  };
}

/// generated route for
/// [VideoDetailScreen]
class VideoDetailRoute extends PageRouteInfo<void> {
  const VideoDetailRoute({List<PageRouteInfo>? children})
      : super(
          VideoDetailRoute.name,
          initialChildren: children,
        );

  static const String name = 'VideoDetailRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [VideoListScreen]
class VideoListRoute extends PageRouteInfo<void> {
  const VideoListRoute({List<PageRouteInfo>? children})
      : super(
          VideoListRoute.name,
          initialChildren: children,
        );

  static const String name = 'VideoListRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
