import 'package:auto_route/auto_route.dart';
import 'package:video_list_app/features/video/presentation/pages/video_detail_screen.dart';
import 'package:video_list_app/features/video/presentation/pages/video_list_screen.dart';

part 'app_route.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'Screen,Route')
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        /// routes go here
        AutoRoute(page: VideoListRoute.page, initial: true),
        AutoRoute(page: VideoDetailRoute.page),
      ];
}
