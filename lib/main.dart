import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_list_app/core/di/service_locator.dart';
import 'package:video_list_app/core/route/app_route.dart';
import 'package:video_list_app/features/video/presentation/bloc/video_bloc.dart';

void main() {
  setupServiceLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => VideoBloc(sl(), sl(), sl())..add(FetchEvent()),
      child: MaterialApp.router(
        title: 'Video List',
        routerConfig: _appRouter.config(),
      ),
    );
  }
}
