import 'package:dartz/dartz.dart';
import 'package:video_list_app/core/error/failure.dart';
import 'package:video_list_app/features/video/data/data_sources/mock/data_providers.dart';
import 'package:video_list_app/features/video/domain/entity/video.dart';
import 'package:video_list_app/features/video/domain/entity/video_response.dart';
import 'package:video_list_app/features/video/domain/repositories/video_base_repository.dart';
import 'package:video_list_app/features/video/domain/use_cases/get_video_use_case.dart';
import 'package:video_list_app/features/video/domain/use_cases/set_like_video_use_case.dart';

class VideoRepositoryImpl implements VideoBaseRepository {
  final VideosDataProvider videosDataProvider;
  final LikesDataProvider likesDataProvider;

  VideoRepositoryImpl({
    required this.videosDataProvider,
    required this.likesDataProvider,
  });

  @override
  Future<Either<Failure, VideosResponse>> fetchVideos(
      FetchVideoParams params) async {
    try {
      final data = await videosDataProvider.getVideos(page: params.page);
      return right(data);
    } catch (error) {
      return left(const ExceptionFailure("Fetch video Error!"));
    }
  }

  @override
  Future<Either<Failure, Video>> setLikeVideo(LikeVideoParams params) async {
    try {
      final data = await likesDataProvider.setLike(id: params.videoId);
      return right(data);
    } catch (error) {
      return left(const ExceptionFailure("Set like Error!"));
    }
  }

  @override
  Future<Either<Failure, Video>> unsetLikeVideo(LikeVideoParams params) async {
    try {
      final data = await likesDataProvider.unsetLike(id: params.videoId);
      return right(data);
    } catch (error) {
      return left(const ExceptionFailure("UnSet like Error!"));
    }
  }
}
