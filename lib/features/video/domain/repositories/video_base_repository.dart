import 'package:dartz/dartz.dart';
import 'package:video_list_app/core/error/failure.dart';
import 'package:video_list_app/features/video/domain/entity/video.dart';
import 'package:video_list_app/features/video/domain/entity/video_response.dart';
import 'package:video_list_app/features/video/domain/use_cases/get_video_use_case.dart';
import 'package:video_list_app/features/video/domain/use_cases/set_like_video_use_case.dart';

abstract class VideoBaseRepository {
  Future<Either<Failure, VideosResponse>> fetchVideos(FetchVideoParams params);

  Future<Either<Failure, Video>> setLikeVideo(LikeVideoParams params);

  Future<Either<Failure, Video>> unsetLikeVideo(LikeVideoParams params);
}
