import 'package:dartz/dartz.dart';
import 'package:video_list_app/core/error/failure.dart';
import 'package:video_list_app/features/video/domain/entity/video.dart';
import 'package:video_list_app/features/video/domain/repositories/video_base_repository.dart';
import 'package:video_list_app/features/video/domain/use_cases/set_like_video_use_case.dart';

class UnSetLikeVideoUseCase {
  final VideoBaseRepository videoBaseRepository;

  UnSetLikeVideoUseCase(this.videoBaseRepository);

  Future<Either<Failure, Video>> call(LikeVideoParams params) async {
    return videoBaseRepository.unsetLikeVideo(params);
  }
}
