import 'package:dartz/dartz.dart';
import 'package:video_list_app/core/error/failure.dart';
import 'package:video_list_app/features/video/domain/entity/video.dart';
import 'package:video_list_app/features/video/domain/repositories/video_base_repository.dart';

class SetLikeVideoUseCase {
  final VideoBaseRepository videoBaseRepository;

  SetLikeVideoUseCase(this.videoBaseRepository);

  Future<Either<Failure, Video>> call(LikeVideoParams params) async {
    return videoBaseRepository.setLikeVideo(params);
  }
}

class LikeVideoParams {
  final int videoId;

  LikeVideoParams(this.videoId);
}
