import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:video_list_app/core/error/failure.dart';
import 'package:video_list_app/features/video/domain/entity/video_response.dart';
import 'package:video_list_app/features/video/domain/repositories/video_base_repository.dart';

class FetchVideoUseCase {
  final VideoBaseRepository videoBaseRepository;

  FetchVideoUseCase(this.videoBaseRepository);

  Future<Either<Failure, VideosResponse>> call(FetchVideoParams params) async {
    log("USER CASE PARAMS: $params");
    return videoBaseRepository.fetchVideos(params);
  }
}

class FetchVideoParams {
  final int page;
  final String? query;

  FetchVideoParams({
    this.page = 1,
    this.query,
  });

  @override
  String toString() {
    return "page: $page, query: $query";
  }
}
