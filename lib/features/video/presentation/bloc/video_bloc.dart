import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_list_app/core/utils/debouncer.dart';
import 'package:video_list_app/core/utils/throtler.dart';
import 'package:video_list_app/core/utils/wrapped_data.dart';
import 'package:video_list_app/features/video/domain/entity/pagination.dart';
import 'package:video_list_app/features/video/domain/entity/video.dart';
import 'package:video_list_app/features/video/domain/use_cases/get_video_use_case.dart';
import 'package:video_list_app/features/video/domain/use_cases/set_like_video_use_case.dart';
import 'package:video_list_app/features/video/domain/use_cases/unset_like_video_use_case.dart';

part 'video_event.dart';

part 'video_state.dart';

const throttleDuration = Duration(milliseconds: 200);
const _debounceDuration = Duration(milliseconds: 700);

class VideoBloc extends Bloc<VideoEvent, VideoState> {
  final FetchVideoUseCase fetchVideoUseCase;
  final SetLikeVideoUseCase setLikeVideoUseCase;
  final UnSetLikeVideoUseCase unSetLikeVideoUseCase;

  VideoBloc(
    this.setLikeVideoUseCase,
    this.unSetLikeVideoUseCase,
    this.fetchVideoUseCase,
  ) : super(const VideoState()) {
    on<FetchEvent>(
      (FetchEvent event, Emitter<VideoState> emit) async {
        log("Fetching video data: $state");
        if (!state.pagination.hasMore) return;
        final failureOrSuccess = await fetchVideoUseCase(
          FetchVideoParams(
            page: state.status == VideoStateStatus.initial
                ? 1
                : state.pagination.page + 1,
          ),
        );

        failureOrSuccess.fold(
          (failure) => emit(
            state.copyWith(
                error: failure.message, status: VideoStateStatus.failed),
          ),
          (success) => emit(
            VideoState(
              data: List.of(state.data)..addAll(success.data),
              main: success.main!,
              pagination: success.pagination,
              status: VideoStateStatus.success,
            ),
          ),
        );
      },
      transformer: throttleDroppable(throttleDuration),
    );

    on<SearchVideo>(
      (SearchVideo event, Emitter<VideoState> emit) async {
        final failureOrSuccess = await fetchVideoUseCase(
          FetchVideoParams(
            query: event.query,
          ),
        );

        failureOrSuccess.fold(
          (failure) => emit(
            state.copyWith(
                error: failure.message,
                queryText: event.query,
                status: VideoStateStatus.failed),
          ),
          (success) => emit(
            VideoState(
                data: success.data,
                main: success.main!,
                pagination: success.pagination,
                status: VideoStateStatus.success),
          ),
        );
      },
      transformer: debounceTransformer(_debounceDuration),
    );

    on<LikeVideoEvent>((LikeVideoEvent event, Emitter<VideoState> emit) async {
      final videos = state.data;
      final videoIndex = videos.indexWhere((item) => item.id == event.videoId);
      if (state.selectedVideo != null) {
        emit(
          state.copyWith(
            selectedVideo: Wrapped.value(
              state.selectedVideo!.copyWith(liked: !state.selectedVideo!.liked),
            ),
          ),
        );
      }
      if (!videos[videoIndex].liked) {
        final failureOrSuccess =
            await setLikeVideoUseCase(LikeVideoParams(event.videoId));
        failureOrSuccess.fold(
          (failure) => emit(
            state.copyWith(error: failure.message),
          ),
          (success) {
            videos[videoIndex] = success;
            emit(state.copyWith(data: videos));
          },
        );
      } else {
        final failureOrSuccess =
            await unSetLikeVideoUseCase(LikeVideoParams(event.videoId));
        failureOrSuccess.fold(
          (failure) => emit(
            state.copyWith(error: failure.message),
          ),
          (success) {
            videos[videoIndex] = success;
            emit(state.copyWith(data: videos));
          },
        );
      }
    });

    on<SelectVideo>((SelectVideo event, Emitter<VideoState> emit) {
      emit(
        state.copyWith(
          selectedVideo: Wrapped.value(
            state.data.firstWhere((item) => item.id == event.videoId),
          ),
          status: VideoStateStatus.detail,
        ),
      );
    });

    on<UnSelectVideo>((UnSelectVideo event, Emitter<VideoState> emit) {
      emit(state.copyWith(
        selectedVideo: const Wrapped.value(null),
        status: VideoStateStatus.initial,
      ));
    });
  }
}
