part of 'video_bloc.dart';

abstract class VideoEvent {}

class FetchEvent extends VideoEvent {}

class LikeVideoEvent extends VideoEvent {
  final int videoId;

  LikeVideoEvent(this.videoId);
}

class SearchVideo extends VideoEvent {
  final String query;
  final int page;

  SearchVideo({
    this.query = "",
    this.page = 0,
  });
}

class SelectVideo extends VideoEvent {
  final int videoId;

  SelectVideo(this.videoId);
}

class UnSelectVideo extends VideoEvent {
  final int videoId;

  UnSelectVideo(this.videoId);
}
