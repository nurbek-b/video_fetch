part of 'video_bloc.dart';

enum VideoStateStatus { initial, failed, success, detail }

class VideoState extends Equatable {
  final VideoStateStatus status;
  final Video? main;
  final List<Video> data;
  final Video? selectedVideo;
  final Pagination pagination;
  final String queryText;
  final String error;

  const VideoState({
    this.status = VideoStateStatus.initial,
    this.main,
    this.data = const [],
    this.selectedVideo,
    this.queryText = "",
    this.pagination = const Pagination(hasMore: true, page: 0),
    this.error = "",
  });

  @override
  List<Object?> get props => [
        status,
        main,
        data,
        pagination,
        error,
        queryText,
        selectedVideo,
      ];

  VideoState copyWith({
    VideoStateStatus? status,
    List<Video>? data,
    Wrapped<Video?>? selectedVideo,
    Wrapped<Video?>? main,
    Pagination? pagination,
    String? queryText,
    String? error,
  }) {
    return VideoState(
      status: status ?? this.status,
      data: data ?? this.data,
      main: main != null ? main.value : this.main,
      selectedVideo:
          selectedVideo != null ? selectedVideo.value : this.selectedVideo,
      pagination: pagination ?? this.pagination,
      queryText: queryText ?? this.queryText,
      error: error ?? this.error,
    );
  }
}
