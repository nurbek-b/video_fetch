import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_list_app/features/video/presentation/bloc/video_bloc.dart';
import 'package:video_list_app/features/video/presentation/widgets/main_video.dart';

@RoutePage()
class VideoDetailScreen extends StatelessWidget {
  const VideoDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<VideoBloc, VideoState>(
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                context
                    .read<VideoBloc>()
                    .add(UnSelectVideo(state.selectedVideo!.id));
              },
            ),
          ),
          body: MainVideoDisplayWidget(video: state.selectedVideo!),
        );
      },
    );
  }
}
