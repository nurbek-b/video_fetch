import 'dart:developer';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_list_app/core/route/app_route.dart';
import 'package:video_list_app/features/video/presentation/bloc/video_bloc.dart';
import 'package:video_list_app/features/video/presentation/widgets/loading_main_page.dart';
import 'package:video_list_app/features/video/presentation/widgets/retry_page.dart';
import 'package:video_list_app/features/video/presentation/widgets/video_list_view.dart';

@RoutePage()
class VideoListScreen extends StatefulWidget {
  const VideoListScreen({super.key});

  @override
  State<VideoListScreen> createState() => _VideoListScreenState();
}

class _VideoListScreenState extends State<VideoListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<VideoBloc, VideoState>(
        listener: (context, state) {
          if (state.selectedVideo != null) {
            context.router.push(const VideoDetailRoute());
          }
        },
        builder: (context, state) {
          switch (state.status) {
            case VideoStateStatus.initial:
              return const LoadingMain();
            case VideoStateStatus.failed:
              return RetryOnErrorPage(queryText: state.queryText);
            case VideoStateStatus.success:
              return const VideoListView();
            case VideoStateStatus.detail:
            default:
              return const SizedBox();
          }
        },
      ),
    );
  }
}
