import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:video_list_app/core/extensions/context_extensions.dart';
import 'package:video_list_app/features/video/presentation/widgets/placeholders.dart';

class LoadingMain extends StatelessWidget {
  const LoadingMain({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: context.screenHeight,
      width: context.screenWidth,
      child: Shimmer.fromColors(
          baseColor: Colors.grey.shade300,
          highlightColor: Colors.grey.shade100,
          enabled: true,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                BannerPlaceholder(
                  height: context.screenHeight * .3,
                  width: context.screenWidth,
                ),
                TitlePlaceholder(width: context.screenWidth),
                const SizedBox(height: 4.0),
                Row(
                  children: [
                    TitlePlaceholder(width: context.screenWidth * .3),
                    TitlePlaceholder(width: context.screenWidth * .2),
                  ],
                ),
                const SizedBox(height: 16.0),
                const Row(
                  children: [
                    SizedBox(width: 20,),
                    Text(
                      "Все видео",
                      style: TextStyle(color: Colors.black),
                    ),
                    Spacer(),
                    Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    SizedBox(width: 20,),
                  ],
                ),
                const SizedBox(height: 16.0),
                ...List.generate(
                  4,
                  (_) => const Row(
                    children: [
                      ContentPlaceholder(),
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }
}
