import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_list_app/core/extensions/context_extensions.dart';
import 'package:video_list_app/features/video/presentation/bloc/video_bloc.dart';
import 'package:video_list_app/features/video/presentation/widgets/bottom_loader.dart';
import 'package:video_list_app/features/video/presentation/widgets/main_video.dart';
import 'package:video_list_app/features/video/presentation/widgets/search_bar.dart';
import 'package:video_list_app/features/video/presentation/widgets/video_list_item.dart';

class VideoListView extends StatefulWidget {
  const VideoListView({super.key});

  @override
  State<VideoListView> createState() => _VideoListViewState();
}

class _VideoListViewState extends State<VideoListView> {
  final _scrollController = ScrollController();
  bool isAppBarExpanded = true;
  var top = 0.0;

  @override
  void initState() {
    super.initState();
    log("UPDATING STATE");
    _scrollController.addListener(_scrollEvent);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<VideoBloc, VideoState>(
      builder: (context, state) {
        return CustomScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          controller: _scrollController,
          slivers: [
            SliverAppBar(
              floating: false,
              pinned: true,
              elevation: .0,
              foregroundColor: Colors.white,
              backgroundColor: Colors.white,
              surfaceTintColor: Colors.white,
              automaticallyImplyLeading: false,
              flexibleSpace: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                top = constraints.biggest.height;
                return FlexibleSpaceBar(
                  title: AnimatedOpacity(
                    duration: const Duration(milliseconds: 200),
                    opacity: top == context.mq.padding.top + kToolbarHeight
                        ? 1.0
                        : 0.0,
                    child: const SizedBox(height: 50, child: SearchFieldBar()),
                  ),
                  background: MainVideoDisplayWidget(
                    video: state.main!,
                    isMain: true,
                  ),
                );
              }),
              expandedHeight: context.screenHeight * .5,
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    const Text("Все видео"),
                    const Spacer(),
                    IconButton(
                      onPressed: () {
                        _scrollController.animateTo(
                          _scrollController.position.minScrollExtent +
                              context.screenHeight * .4,
                          duration: const Duration(milliseconds: 500),
                          curve: Curves.decelerate,
                        );
                      },
                      icon: const Icon(Icons.search_rounded),
                    ),
                  ],
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (context, index) {
                  return state.data.length > index
                      ? VideoListItem(
                          video: state.data[index],
                        )
                      : const BottomLoader();
                },
                childCount: state.pagination.hasMore
                    ? state.data.length + 1
                    : state.data.length,
              ),
            ),
          ],
        );
      },
    );
  }

  void _scrollEvent() {
    if (_isBottom) {
      log("it hits is bottom");
      context.read<VideoBloc>().add(FetchEvent());
    }
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_scrollEvent)
      ..dispose();
    super.dispose();
  }
}
