import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:video_list_app/core/extensions/context_extensions.dart';
import 'package:video_list_app/features/video/domain/entity/video.dart';
import 'package:video_list_app/features/video/presentation/bloc/video_bloc.dart';

class VideoListItem extends StatelessWidget {
  final Video video;

  const VideoListItem({
    super.key,
    required this.video,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        context.read<VideoBloc>().add(SelectVideo(video.id));
      },
      contentPadding:
          const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      leading: CachedNetworkImage(
        height: context.screenHeight * .3,
        width: context.screenWidth * .35,
        fit: BoxFit.cover,
        imageUrl: video.image,
        placeholder: (context, url) => const CircularProgressIndicator(),
        errorWidget: (context, url, error) => const Center(
          child: Icon(Icons.error),
        ),
      ),
      title: Text(
        video.name,
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "${video.views} просмотров",
                overflow: TextOverflow.ellipsis,
              ),
              Text("${video.duration}"),
            ],
          ),
          const Spacer(),
          Row(
            children: [
              video.liked
                  ? InkWell(
                      onTap: () {
                        context.read<VideoBloc>().add(
                              LikeVideoEvent(
                                video.id,
                              ),
                            );
                      },
                      child: SvgPicture.asset("assets/filled_heart.svg"))
                  : InkWell(
                      onTap: () {
                        context.read<VideoBloc>().add(
                              LikeVideoEvent(
                                video.id,
                              ),
                            );
                      },
                      child: SvgPicture.asset("assets/empty_heart.svg")),
              if (video.likes > 0) Text("${video.likes}")
            ],
          )
        ],
      ),
    );
  }
}
