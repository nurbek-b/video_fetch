import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:video_list_app/features/video/domain/entity/video.dart';
import 'package:video_list_app/features/video/presentation/bloc/video_bloc.dart';

class MainVideoDisplayWidget extends StatelessWidget {
  final Video video;
  final bool isMain;

  const MainVideoDisplayWidget({
    super.key,
    required this.video,
    this.isMain = false,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (isMain)
          Container(
            height: 100,
            color: Colors.white,
            alignment: const Alignment(0, .6),
            child: const Text(
              "Список видео",
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
          ),
        CachedNetworkImage(
          fit: BoxFit.fitWidth,
          imageUrl: video.image,
          placeholder: (context, url) => const CircularProgressIndicator(),
          errorWidget: (context, url, error) => const Center(
            child: Icon(Icons.error),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(14),
          child: Text(
            video.name,
            style: const TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 14),
          child: Row(
            children: [
              Text("${video.views} просмотров"),
              const Spacer(),
              Text("${video.duration}"),
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        video.liked
            ? InkWell(
                onTap: () {
                  context.read<VideoBloc>().add(
                        LikeVideoEvent(
                          video.id,
                        ),
                      );
                },
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: SvgPicture.asset("assets/filled_heart.svg"),
                ))
            : InkWell(
                onTap: () {
                  context.read<VideoBloc>().add(
                        LikeVideoEvent(
                          video.id,
                        ),
                      );
                },
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: SvgPicture.asset("assets/empty_heart.svg"),
                )),
      ],
    );
  }
}
