import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_list_app/features/video/presentation/bloc/video_bloc.dart';

class RetryOnErrorPage extends StatelessWidget {
  final String? queryText;

  const RetryOnErrorPage({
    super.key,
    this.queryText,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: TextButton(
        child: const Text("Попробовать еще раз!"),
        onPressed: () {
          if (queryText != null) {
            context.read<VideoBloc>().add(FetchEvent());
          } else {
            context.read<VideoBloc>().add(SearchVideo(query: queryText ?? ""));
          }
        },
      ),
    );
  }
}
