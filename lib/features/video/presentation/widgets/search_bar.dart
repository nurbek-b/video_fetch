import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_list_app/core/extensions/context_extensions.dart';
import 'package:video_list_app/features/video/presentation/bloc/video_bloc.dart';

class SearchFieldBar extends StatefulWidget {
  const SearchFieldBar({super.key});

  @override
  State<SearchFieldBar> createState() => _SearchFieldBarState();
}

class _SearchFieldBarState extends State<SearchFieldBar> {
  late TextEditingController textController;

  @override
  void initState() {
    textController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
        SizedBox(
          width: context.screenWidth * .8,
          child: TextField(
            controller: textController,
            onChanged: (value) {
              context.read<VideoBloc>().add(SearchVideo());
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              border: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.black),
                borderRadius: BorderRadius.circular(20),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.black),
                borderRadius: BorderRadius.circular(20),
              ),
              hintText: "Поиск видео",
              prefixIcon: const Icon(Icons.search),
              suffixIcon: IconButton(
                icon: const Icon(Icons.close_outlined),
                onPressed: () {
                  textController.clear();
                },
              ),
              suffixIconColor: Colors.black,
              prefixIconColor: Colors.black,
            ),
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }
}
